# hipaa-anon

This is a simple little Luminus project that handles anonymizing
patient records.

The purpose of this project is to ingest a patient record, defined
thusly:

``` clojure
(def PatientRecord
  {:birthDate     string?
   :zipCode       string?
   :admissionDate string?
   :dischargeDate string?
   :notes         string?})
```

and produce a "de-identified" version of this record by masking the
birth date, ZIP code, admission date, discharge date, and notes.

## How it Works

The de-identification of records sent in is primarily handled in the
`hipaa-anon.handlers.anonymize` namespace, wherein a set of `masks` is
applied to the incoming payload in order to produce an anonymized
version thereof.

The incoming request is routed in `hipaa-anon.routes.services` under
`/api/anonymize`:

``` clojure
(defn service-routes []
  ["/api"
   ;; ...
   ["/anonymize"
    {:post {:summary    "Anonymize the given patient record"
            :parameters {:body PatientRecord}
            :responses  {200 {:body AnonymizedPatientRecord}}
            :handler    anonymize}}]])
```

This route takes a `PatientRecord` (described above) and passes it to
the `hipaa-anon.handlers.anonymize/anonymize` function:

``` clojure
(defn anonymize
  "Anonymize the given patient record."
  [{{body :body} :parameters}]
  {:status 200
   :body   (mask-record body)})
```

`mask-record` is a function that simply `reduce`s `apply-mask` over
the defined `masks` over the given payload. Entries here each contain
an `old-key`, `new-key`, and `mask-fn`, e.g.

``` clojure
{:old-key :birthDate
 :new-key :age
 :mask-fn mask-birthdate}
```

`apply-mask` extracts `old-key` from the payload (if present;
otherwise nothing occurs), applies the `mask-fn` to that value, and
inserts it into the resulting response as `new-key`, replacing the old
entry entirely:

``` clojure
(defn apply-mask
  "Apply a mask operation to the given `rec`.

  This replaces the unmasked entry in `rec` (denoted by `old-key`)
  with a masked version assigned to the key specified in `new-key`."
  [rec {:keys [old-key new-key mask-fn]}]
  (if-let [val (get rec old-key)]
    (-> rec (dissoc old-key) (assoc new-key (mask-fn val)))
    rec))
```

These functions perform various operations on the values they are
given:

### Birth Date

``` clojure
(defn date->age
  "Compute the age, in years, of the given date."
  [date-str]
  (let [today     (l/local-now)
        birthdate (parse-date date-str)
        delta     (t/interval birthdate today)]
    (t/in-years delta)))

(def max-age 90)

(defn mask-birthdate
  "Mask a birth date and return an age.

  This simply returns the age represented by the given birth date with
  a maximum of 90 years; all patients over 89 years old are grouped
  into a 90+ age."
  [date-str]
  (min max-age (date->age date-str)))
```

`mask-birthdate` first parses the given date string into an instance
of `org.joda.time.DateTime`. It then uses `date->age` to compute the
age represented by the given date (as compared to `l/local-now`) and
returns the `min` of `max-age` and the resulting number of years to
cap it at 90.

### ZIP Code

``` clojure
(def zip-prefix-len 3)
(def zip-population-cutoff 20000)
(def default-zip-code "00000")

(defn mask-zip
  "Mask patient ZIP code.

  This first extracts a `zip-prefix-len`-digit prefix from the ZIP
  code. If the combined population of all ZIP codes starting with this
  prefix is less than `zip-population-cutoff`, '00000' is
  returned. Otherwise, the first `zip-prefix-len` digits are returned
  with the remaining digits set to 0."
  [zip]
  (let [prefix     (subs zip 0 zip-prefix-len)
        population (population-by-prefix prefix)]
    (if (>= population zip-population-cutoff)
      (str prefix "00")
      default-zip-code)))
```

`mask-zip` first extracts a prefix of `zip-prefix-len` digits from the
given ZIP code and calls `population-by-prefix` to check the
population of all ZIPs with that prefix. If it is at least
`zip-population-cutoff`, only the final two digits are replaced
with 0. Otherwise, `default-zip-code` (in this case "00000") is
returned instead.

The ZIP code database is loaded from the provided
`resources/zips.csv`.

### Admission/Discharge Dates

Masking of these two fields is done simply by extracting the year from
the date parsed from the given string:

``` clojure
(defn date->year
  "Extract the year from the given `date-str`.

  NB: This could've been referenced, for consistency, in a `(def
  mask-admission-date date->year)`, etc. but I did not want to clog up
  the namespace with such `def`s (and because this is used
  elsewhere)."
  [date-str]
  (.getYear (parse-date date-str)))
```

### Notes

Masking of the `notes` field is split up into two passes:

``` clojure
(defn mask-notes
  "Mask the `notes` entry in the patient record.

  This replaces instances of SSNs, dates, phone numbers, and email
  addresses."
  [notes]
  (-> notes
      apply-notes-regexp-replacements
      apply-notes-date-replacements))
```

#### Phone, Email, and SSN

This operation replaces all instances of the following regexes with
provided replacements:

``` clojure
(def notes-replacements
  {:ssn           [#"\d{3}-\d{2}-\d{4}" "XXX-XX-XXXX"]
   :phone-number  [#"(\(\d{3}\) |\d{3}-)\d{3}-\d{4}" "XXX-XXX-XXXX"]
   :email-address [#"[A-Za-z0-9][A-Za-z0-9\.\-_]*@[A-Za-z0-9][A-Za-z0-9\-_\.]*\.[A-Za-z]+" "xxx@xxx.xx"]})
```

e.g. anything matching `/\d{3}-\d{2}-\d{4}/` for a social security
number would be replaced with `XXX-XX-XXXX`. Each of these regex
definitions is consumed in a `loop`/`recur` and collected into a final
first-stage masked `notes` field:

``` clojure
(defn apply-notes-regexp-replacements
  "Apply basic regex replacements to the given `notes`.

  This tries to mask instances of SSNs, phone numbers, and email
  addresses."
  [notes]
  (loop [[[regexp replacement] & more] (vals notes-replacements)
         masked                        notes]
    (let [result (string/replace masked regexp replacement)]
      (if more
        (recur more result)
        result))))
```

#### Dates to Years

This operation is fairly similar and simply `loop`s over all matches
of the `date-re` (here `/\d{4}-\d{2}-\d{2}/`), replacing each with the
result of `date->year` to extract and replace into the result the year
represented by each date:

``` clojure
(defn apply-notes-date-replacements
  "Replace all dates (of the form specified in `date-re`,
  i.e. `YYYY-MM-DD`) with just the year."
  [notes]
  (loop [[date-match & more] (re-seq date-re notes)
         masked              notes]
    (let [year   (date->year date-match)
          result (string/replace masked date-match (str year))]
      (if more
        (recur more result)
        result))))
```

## Prerequisites

Just a modern version of the JRE

## Running

The latest JAR is included in this repository as `hipaa-anon.jar`. It
may be run as follows:

``` shell
java -jar hipaa-anon.jar
```

This should start the server on port 3000. At this point, you should
be able to reach the most important route, which is
`/api/anonymize`. To anonymize a patient record, simply send a POST
containing a JSON payload adhering to the schema above, e.g. with a
payload like

``` json
{
  "birthDate": "1993-06-23",
  "zipCode": "19151",
  "admissionDate": "2019-01-04",
  "dischargeDate": "2019-01-06",
  "notes": "Admitted on 2019-01-04, discharged 2019-01-06, SSN 123-45-6789, DOB 1993-06-23, phone (844) 931-0234 with backup 399-299-2034, and email abc@abc.xyz"
}
```

as follows:

``` shell
curl -XPOST -d @payload.json -H 'Content-type: application/json' http://localhost:3000/api/anonymize
```

which should produce

``` json
{
  "age": 27,
  "zipCode": "19100",
  "admissionYear": 2019,
  "dischargeYear": 2019,
  "notes": "Admitted on 2019, discharged 2019, SSN XXX-XX-XXXX, DOB 1993, phone XXX-XXX-XXXX with backup XXX-XXX-XXXX, and email xxx@xxx.xx"
}
```

**NB:** Please keep in mind that response bodies will include the
`age`, `admissionYear`, and `dischargeYear` as `Integer` rather than
`String` (as in the example). If `String` is preferred, you may simply
add a call to `str` in `hipaa-anon.handlers.anonymize/apply-mask` and
alter the `hipaa-anon.routes.services/AnonymizedPatientRecord` schema
to accept the new value types. A diff for this is as follows:

``` diff
diff --git a/src/clj/hipaa_anon/handlers/anonymize.clj b/src/clj/hipaa_anon/handlers/anonymize.clj
index 2bb70bb..e124d87 100644
--- a/src/clj/hipaa_anon/handlers/anonymize.clj
+++ b/src/clj/hipaa_anon/handlers/anonymize.clj
@@ -144,7 +144,7 @@
   with a masked version assigned to the key specified in `new-key`."
   [rec {:keys [old-key new-key mask-fn]}]
   (if-let [val (get rec old-key)]
-    (-> rec (dissoc old-key) (assoc new-key (mask-fn val)))
+    (-> rec (dissoc old-key) (assoc new-key (str (mask-fn val))))
     rec))

 (defn mask-record
diff --git a/src/clj/hipaa_anon/routes/services.clj b/src/clj/hipaa_anon/routes/services.clj
index 38980ad..aed8634 100644
--- a/src/clj/hipaa_anon/routes/services.clj
+++ b/src/clj/hipaa_anon/routes/services.clj
@@ -30,10 +30,10 @@
 ;; to `hipaa-anon.handlers.anonymize.apply-mask` and altering this
 ;; schema to be entirely `string?`.
 (def AnonymizedPatientRecord
-  {:age           int?
+  {:age           string?
    :zipCode       string?
-   :admissionYear int?
-   :dischargeYear int?
+   :admissionYear string?
+   :dischargeYear string?
    :notes         string?})

 (defn service-routes []
```

After making these changes, you can recompile with `lein uberjar` and
run that JAR instead. These changes would produce a response like the
following:

``` json
{
  "age": "27",
  "zipCode": "19100",
  "admissionYear": "2019",
  "dischargeYear": "2019",
  "notes": "Admitted on 2019, discharged 2019, SSN XXX-XX-XXXX, DOB 1993, phone XXX-XXX-XXXX with backup XXX-XXX-XXXX, and email xxx@xxx.xx"
}
```

## Notes

This project was generated using [Luminus](https://luminusweb.com/)
and as such contains a lot of boilerplate. The meat and potatoes of
this project is found primarily in the following files:

- `src/clj/hipaa_anon/handlers/anonymize.clj` -- the anonymize code
- `src/clj/hipaa_anon/helpers.clj` -- a few helpers
- `src/clj/hipaa_anon/zip_db.clj` -- ZIP population database

## Development

### Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

### Running

To start a web server for the application, run:

    lein run

At this point, you should be able to reach the web app on port 3000
and an nREPL connection on port 7000.

### Building

To build a JAR, simply run `lein uberjar`. This will produce a JAR
file in `target/uberjar` that can be run using `java -jar` as above.
