(ns hipaa-anon.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [hipaa-anon.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[hipaa-anon started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[hipaa-anon has shut down successfully]=-"))
   :middleware wrap-dev})
