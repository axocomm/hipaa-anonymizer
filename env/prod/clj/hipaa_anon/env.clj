(ns hipaa-anon.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[hipaa-anon started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[hipaa-anon has shut down successfully]=-"))
   :middleware identity})
