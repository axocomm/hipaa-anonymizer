(ns hipaa-anon.handlers.anonymize
  (:require
   [clojure.string :as string]
   [clj-time.core :as t]
   [clj-time.format :as f]
   [clj-time.local :as l]
   [hipaa-anon.zip-db :refer [population-by-prefix]]))

;;;;;;;;;;;;;
;; Helpers ;;
;;;;;;;;;;;;;

(def date-formatter (f/formatters :year-month-day))

(defn parse-date
  "Parse the given date string into a `DateTime` object using the format
  specified by `date-formatter`."
  [date-str]
  (f/parse date-formatter date-str))

;;;;;;;;;;;;;
;; Masking ;;
;;;;;;;;;;;;;

;; Birth, admission, and discharge dates.

(defn date->age
  "Compute the age, in years, of the given date."
  [date-str]
  (let [today     (l/local-now)
        birthdate (parse-date date-str)
        delta     (t/interval birthdate today)]
    (t/in-years delta)))

(def max-age 90)

(defn mask-birthdate
  "Mask a birth date and return an age.

  This simply returns the age represented by the given birth date with
  a maximum of 90 years; all patients over 89 years old are grouped
  into a 90+ age."
  [date-str]
  (min max-age (date->age date-str)))

(defn date->year
  "Extract the year from the given `date-str`.

  NB: This could've been referenced, for consistency, in a `(def
  mask-admission-date date->year)`, etc. but I did not want to clog up
  the namespace with such `def`s (and because this is used
  elsewhere)."
  [date-str]
  (.getYear (parse-date date-str)))

;; ZIP code.

(def zip-prefix-len 3)
(def zip-population-cutoff 20000)
(def default-zip-code "00000")

(defn mask-zip
  "Mask patient ZIP code.

  This first extracts a `zip-prefix-len`-digit prefix from the ZIP
  code. If the combined population of all ZIP codes starting with this
  prefix is less than `zip-population-cutoff`, '00000' is
  returned. Otherwise, the first `zip-prefix-len` digits are returned
  with the remaining digits set to 0."
  [zip]
  (let [prefix     (subs zip 0 zip-prefix-len)
        population (population-by-prefix prefix)]
    (if (>= population zip-population-cutoff)
      (str prefix "00")
      default-zip-code)))

;; Notes.

(def notes-replacements
  {:ssn           [#"\d{3}-\d{2}-\d{4}" "XXX-XX-XXXX"]
   :phone-number  [#"(\(\d{3}\) |\d{3}-)\d{3}-\d{4}" "XXX-XXX-XXXX"]
   :email-address [#"[A-Za-z0-9][A-Za-z0-9\.\-_]*@[A-Za-z0-9][A-Za-z0-9\-_\.]*\.[A-Za-z]+" "xxx@xxx.xx"]})

(defn apply-notes-regexp-replacements
  "Apply basic regex replacements to the given `notes`.

  This tries to mask instances of SSNs, phone numbers, and email
  addresses."
  [notes]
  (loop [[[regexp replacement] & more] (vals notes-replacements)
         masked                        notes]
    (let [result (string/replace masked regexp replacement)]
      (if more
        (recur more result)
        result))))

(def date-re #"\d{4}-\d{2}-\d{2}")

(defn apply-notes-date-replacements
  "Replace all dates (of the form specified in `date-re`,
  i.e. `YYYY-MM-DD`) with just the year."
  [notes]
  (loop [[date-match & more] (re-seq date-re notes)
         masked              notes]
    (let [year   (date->year date-match)
          result (string/replace masked date-match (str year))]
      (if more
        (recur more result)
        result))))

(defn mask-notes
  "Mask the `notes` entry in the patient record.

  This replaces instances of SSNs, dates, phone numbers, and email
  addresses."
  [notes]
  (-> notes
      apply-notes-regexp-replacements
      apply-notes-date-replacements))

;; Record masking.

(def masks
  [{:old-key :birthDate
    :new-key :age
    :mask-fn mask-birthdate}
   {:old-key :zipCode
    :new-key :zipCode
    :mask-fn mask-zip}
   {:old-key :admissionDate
    :new-key :admissionYear
    :mask-fn date->year}
   {:old-key :dischargeDate
    :new-key :dischargeYear
    :mask-fn date->year}
   {:old-key :notes
    :new-key :notes
    :mask-fn mask-notes}])

(defn apply-mask
  "Apply a mask operation to the given `rec`.

  This replaces the unmasked entry in `rec` (denoted by `old-key`)
  with a masked version assigned to the key specified in `new-key`."
  [rec {:keys [old-key new-key mask-fn]}]
  (if-let [val (get rec old-key)]
    (-> rec (dissoc old-key) (assoc new-key (mask-fn val)))
    rec))

(defn mask-record
  "Apply masks defined in `masks` to the given patient record."
  [rec]
  (reduce apply-mask rec masks))

(defn anonymize
  "Anonymize the given patient record."
  [{{body :body} :parameters}]
  {:status 200
   :body   (mask-record body)})
