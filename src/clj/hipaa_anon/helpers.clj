(ns hipaa-anon.helpers)

(defn starts-with?
  "Check if a string starts with the given prefix."
  [prefix string]
  (as-> prefix $
    (format "^%s" $)
    (re-pattern $)
    (re-find $ string)))

(defn parse-int
  "Parse an integer string and return nil if it cannot be parsed."
  [s]
  (try
    (Integer/parseInt s)
    (catch NumberFormatException _
      nil)))
