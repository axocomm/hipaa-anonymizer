(ns hipaa-anon.routes.services
  (:require
   [reitit.swagger :as swagger]
   [reitit.swagger-ui :as swagger-ui]
   [reitit.ring.coercion :as coercion]
   [reitit.coercion.spec :as spec-coercion]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [reitit.ring.middleware.multipart :as multipart]
   [reitit.ring.middleware.parameters :as parameters]
   [hipaa-anon.middleware.formats :as formats]
   [hipaa-anon.middleware.exception :as exception]
   [ring.util.http-response :refer :all]
   [clojure.java.io :as io]
   [hipaa-anon.handlers.anonymize :refer [anonymize]]))

;; Types

(def PatientRecord
  {:birthDate     string?
   :zipCode       string?
   :admissionDate string?
   :dischargeDate string?
   :notes         string?})

;; NB: The example in the challenge document has everything as string,
;; but I was not sure how strict that was. Assuming that was not
;; *precisely* what was expected, this response does not turn age,
;; admissionYear, etc. into strings. However, that is entirely
;; possible if necessary by simply adding a further `(str ...)` call
;; to `hipaa-anon.handlers.anonymize.apply-mask` and altering this
;; schema to be entirely `string?`.
(def AnonymizedPatientRecord
  {:age           int?
   :zipCode       string?
   :admissionYear int?
   :dischargeYear int?
   :notes         string?})

(defn service-routes []
  ["/api"
   {:coercion   spec-coercion/coercion
    :muuntaja   formats/instance
    :swagger    {:id ::api}
    :middleware [;; query-params & form-params
                 parameters/parameters-middleware
                 ;; content-negotiation
                 muuntaja/format-negotiate-middleware
                 ;; encoding response body
                 muuntaja/format-response-middleware
                 ;; exception handling
                 exception/exception-middleware
                 ;; decoding request body
                 muuntaja/format-request-middleware
                 ;; coercing response bodys
                 coercion/coerce-response-middleware
                 ;; coercing request parameters
                 coercion/coerce-request-middleware
                 ;; multipart
                 multipart/multipart-middleware]}

   ;; swagger documentation
   ["" {:no-doc  true
        :swagger {:info {:title       "my-api"
                         :description "https://cljdoc.org/d/metosin/reitit"}}}

    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
            {:url    "/api/swagger.json"
             :config {:validator-url nil}})}]]

   ["/ping"
    {:get (constantly (ok {:message "pong"}))}]

   ["/anonymize"
    {:post {:summary    "Anonymize the given patient record"
            :parameters {:body PatientRecord}
            :responses  {200 {:body AnonymizedPatientRecord}}
            :handler    anonymize}}]])
