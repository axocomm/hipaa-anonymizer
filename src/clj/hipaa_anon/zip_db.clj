(ns hipaa-anon.zip-db
  (:require
   [clojure.java.io :as io]
   [clojure.data.csv :as csv]
   [hipaa-anon.helpers :refer [starts-with? parse-int]]))

(def zip-pops-filename "zips.csv")
(def columns [:zcta :population])

(defn zip-pops
  "Read the ZIP code population data from CSV."
  [filename]
  (with-open [in-file (io/reader (io/resource filename))]
    (let [rows (rest (doall (csv/read-csv in-file)))]
      (map (partial zipmap columns) rows))))

(def zip-populations (zip-pops zip-pops-filename))

(defn population-by-prefix
  "Calculate the total population of ZIP codes starting with the given
  `prefix`."
  [prefix]
  (if-let [matches (filter #(starts-with? prefix (:zcta %))
                           zip-populations)]
    (apply + (map (comp parse-int :population) matches))
    0))
